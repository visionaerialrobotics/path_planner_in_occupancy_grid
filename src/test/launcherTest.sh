#!/bin/bash
#--tab --title "Hector Slam" --command "bash -c \"
#roslaunch ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/launch_files/hector_slam.launch --wait drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK};
           #exec bash\""  \
#If you are executing the tester and it is failing, you can prove launching hector slam.
gnome-terminal  \
--tab --title "Move Base" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/launch_files/move_base.launch --wait drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK};
            exec bash\""  \
--tab --title "Move Base Monitor"  --command "bash -c \"
roslaunch path_planner_in_occupancy_grid path_planner_in_occupancy_grid.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK} debug:=true;
exec bash\"" \&


