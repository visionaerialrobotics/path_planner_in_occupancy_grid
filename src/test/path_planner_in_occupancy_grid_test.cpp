/*!*******************************************************************************************
 *  \file       path_planner_in_occupancy_grid_test.cpp
 *  \brief      path planner in occupancy grid test.
 *  \details    This file contains the PathPlannerInOccupancyGrid implementattion of the test.
 *  \authors    Raul Cruz
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include <gtest/gtest.h>
#include <string>

#include <queue>

// ROS
#include <ros/ros.h>

// Move base
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <thread>
// Aerostack
#include <robot_process.h>
#include <droneMsgsROS/GeneratePath.h>
#include <droneMsgsROS/PathWithID.h>
#include <nav_msgs/Path.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/PathWithID.h>
#include <droneMsgsROS/GeneratePath.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/dronePositionRefCommand.h>
#include <droneMsgsROS/droneCommand.h>
#include <yaml-cpp/yaml.h>

/*
 This test proves that path_planner_in_occupancy_grid works correctly by looking to the topics where it publish.
*/

bool finished=false;
bool x = true;
bool y = true;
bool z = true;
bool len = true;
int finishTest=0;
void spinnerThread(){
    while(!finished){
        ros::spinOnce();
        
    }

}
//This is a callback for the topic path_with_id. Here it is proved that the poses are more than 50, and each one is between some parameters.
//this parameters depend on the position sent in the service.
//Ej: We are in 0,0,0.5 and here we want to go to 0,4,0.5 knowing that this is a straight line. If the X in the poses is more than 0.5 or less than -0.5, there is an error there. 
void chatterCallback(const droneMsgsROS::PathWithID &resp_path)
{
  x = true;
  y = true;
  z = true;
  len = true;
  int i=0;
  finishTest=0;
  int size = resp_path.poses.size();
  if(size<50){
     len=false;
  }
  while(i<size){
    if(resp_path.poses[i].pose.position.x>1 or resp_path.poses[i].pose.position.x<-1){
       x=false;
    }
    if(resp_path.poses[i].pose.position.y>4.5 or resp_path.poses[i].pose.position.y<-0.5){
       y=false;
    }
    if(resp_path.poses[i].pose.position.z>1 or resp_path.poses[i].pose.position.z<0){
       z=false;
    }
    i+=1;
  }
}
TEST(PathPlannerInOccupancyGridTests, topicTest)
{
finishTest=1;
ros::NodeHandle node_handle;   
ros::ServiceClient path_client; 
ros::ServiceServer goal_server;
ros::Subscriber path_sub;
std::cout <<"Test1 Goal:(0,4,0.5)"<<std::endl;
path_sub = node_handle.subscribe("/drone11/path_with_id", 1000, chatterCallback);
path_client = node_handle.serviceClient<droneMsgsROS::GeneratePath>("/drone11/generate_path");
droneMsgsROS::GeneratePath path_msg;
geometry_msgs::PoseStamped target_stamped;
target_stamped.pose.position.x = 0;
target_stamped.pose.position.y = 4;
target_stamped.pose.position.z = 0.5;
path_msg.request.goal = target_stamped;
path_client.call(path_msg);
std::cout <<"Waiting to path_with_id"<<std::endl;
while (finishTest!=0){}
EXPECT_TRUE(x and y and z and len);
path_sub.shutdown();
}
//This is a callback for the topic path_with_id. Here it is proved that the poses are more than 50, and each one is between some parameters.
//this parameters depend on the position sent in the service. In this test the goal is 4,0,0.5.
void chatterCallback2(const droneMsgsROS::PathWithID &resp_path)
{
  x = true;
  y = true;
  z = true;
  len = true;
  int i=0;
  finishTest=0;
  int size = resp_path.poses.size();
  if(size<50){
     len=false;
  }
  while(i<size){
    if(resp_path.poses[i].pose.position.y>1 or resp_path.poses[i].pose.position.y<-0.5){
       y=false;
    }
    if(resp_path.poses[i].pose.position.x>4.5 or resp_path.poses[i].pose.position.x<-0.5){
       x=false;
    }
    if(resp_path.poses[i].pose.position.z>1 or resp_path.poses[i].pose.position.z<0){
       z=false;
    }
    i+=1;
  }
}
TEST(PathPlannerInOccupancyGridTests, topicTest2)
{
finishTest=1;
ros::NodeHandle node_handle;   
ros::ServiceClient path_client; 
ros::ServiceServer goal_server;
ros::Subscriber path_sub;
std::cout <<"Test2 Goal:(4,0,0.5)"<<std::endl;
path_sub = node_handle.subscribe("/drone11/path_with_id",1000, chatterCallback2);
path_client = node_handle.serviceClient<droneMsgsROS::GeneratePath>("/drone11/generate_path");
droneMsgsROS::GeneratePath path_msg;
geometry_msgs::PoseStamped target_stamped;
target_stamped.pose.position.x = 4;
target_stamped.pose.position.y = 0;
target_stamped.pose.position.z = 0.5;
path_msg.request.goal = target_stamped;
path_client.call(path_msg);
std::cout <<"Waiting to path_with_id"<<std::endl;
while (finishTest!=0){}
EXPECT_TRUE(x and y and z and len);
path_sub.shutdown();
}
//This is a callback for the topic path_with_id. Here it is proved that the poses are more than 50, and each one is between some parameters.
//this parameters depend on the position sent in the service. In this test the goal is 4,4,0.5.
void chatterCallback3(const droneMsgsROS::PathWithID &resp_path)
{
  x = true;
  y = true;
  z = true;
  len = true;
  int i=0;
  finishTest=0;
  int size = resp_path.poses.size();
  if(size<50){
     len=false;
  }
  while(i<size){
    if(resp_path.poses[i].pose.position.x>4.5 or resp_path.poses[i].pose.position.x<-0.5){
       x=false;
    }
    if(resp_path.poses[i].pose.position.y>4.5 or resp_path.poses[i].pose.position.y<-0.5){
       y=false;
    }
    if(resp_path.poses[i].pose.position.z>1 or resp_path.poses[i].pose.position.z<0){
       z=false;
    }
    i+=1;
  }
}
TEST(PathPlannerInOccupancyGridTests, topicTest3)
{
finishTest=1;
ros::NodeHandle node_handle;   
ros::ServiceClient path_client; 
ros::ServiceServer goal_server;
ros::Subscriber path_sub;
std::cout <<"Test3 Goal:(4,4,0.5)"<<std::endl;
path_sub = node_handle.subscribe("/drone11/path_with_id",1000, chatterCallback3);
path_client = node_handle.serviceClient<droneMsgsROS::GeneratePath>("/drone11/generate_path");
droneMsgsROS::GeneratePath path_msg;
geometry_msgs::PoseStamped target_stamped;
target_stamped.pose.position.x = 4;
target_stamped.pose.position.y = 4;
target_stamped.pose.position.z = 0.5;
path_msg.request.goal = target_stamped;
path_client.call(path_msg);
std::cout <<"Waiting to path_with_id"<<std::endl;
while (finishTest!=0){}
EXPECT_TRUE(x and y and z and len);
path_sub.shutdown();
finished=true;
}



int main(int argc, char** argv){
    system("rosservice call /drone11/path_planner_in_occupancy_grid/start");
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, ros::this_node::getName());
    std::thread thr(&spinnerThread);
    return RUN_ALL_TESTS();


}

